import dog from './dog.jpeg'
import PhotoItem from "./components/PhotoItem";
import './App.css';

function App() {
        const items = [
                {itemName: "인절미1", itemCount: 1},
                {itemName: "인절미2", itemCount: 2},
                {itemName: "인절미3", itemCount: 3},
                {itemName: "인절미4", itemCount: 4},
                {itemName: "인절미5", itemCount: 5}
        ];
    return (
        <div className="gb-container">
                {items.map((item, index) => (
                    <PhotoItem key={index} itemName={item.itemName} itemCount={item.itemCount} />
                ))}
        </div>

    );
}

export default App;
