import React from 'react';

class PhotoItem extends React.Component {
    render() {
        const {itemName, itemCount} = this.props;

        return (
            <div>
                <div>이름: {itemName}</div>
                <div>나이: {itemCount}</div>
            </div>
        );
    }
}

export default PhotoItem;